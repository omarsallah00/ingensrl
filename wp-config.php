<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file viene utilizzato, durante l’installazione, dallo script
 * di creazione di wp-config.php. Non è necessario utilizzarlo solo via
 * web, è anche possibile copiare questo file in «wp-config.php» e
 * riempire i valori corretti.
 *
 * Questo file definisce le seguenti configurazioni:
 *
 * * Impostazioni MySQL
 * * Prefisso Tabella
 * * Chiavi Segrete
 * * ABSPATH
 *
 * È possibile trovare ultetriori informazioni visitando la pagina del Codex:
 *
 * @link https://codex.wordpress.org/it:Modificare_wp-config.php
 *
 * È possibile ottenere le impostazioni per MySQL dal proprio fornitore di hosting.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - È possibile ottenere queste informazioni dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'wordpress');

/** Nome utente del database MySQL */
define('DB_USER', 'root');

/** Password del database MySQL */
define('DB_PASSWORD', '');

/** Hostname MySQL  */
define('DB_HOST', 'localhost');

/** Charset del Database da utilizzare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8mb4');

/** Il tipo di Collazione del Database. Da non modificare se non si ha idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * È possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * È possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ciò forzerà tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':jJoB[wR=[c07#Y#Ux8L^B5(H1#S(ii5RvK=3i5)FGNX<^xWX`99h:+Ay+-/O&={');
define('SECURE_AUTH_KEY',  'yUCi)ynkb]kaZ@ua]wFp~73fp`d581Hq>DkZhU14p8jrIv.fKg?:FM!FLs=5$3w?');
define('LOGGED_IN_KEY',    '7HiTGf6{4xl?YWd,^p2bXso#5oR>@zX/n[oMW7F%C+Bz%QNgq2):kK9SOcE)>eE9');
define('NONCE_KEY',        '1lgV.;uq#{$X @bi]Nk{1+;!#yD]h3+Jv%Wo9Af,@[<Mv-ty<`=c?baVc5rNKo}O');
define('AUTH_SALT',        'gh}3U>o,IoST|JC~<{zDTwrg3 >@4NX}q} %!CN;QA6(W]X)RQXfxdU[56[l[q:|');
define('SECURE_AUTH_SALT', '6ccCi<K6mH0JtAX%p9)c`6fHvX69]D&[*`OhI#2za;$,!#HKLzaTC$XYUPH-eR-!');
define('LOGGED_IN_SALT',   'g ~5i +g*_)}Z^esL|a,j)]-Gr.oV:?R%/]l76tK~HC$:5C`O>os8F+gd][=vp?j');
define('NONCE_SALT',       '|[.i<1:#>+7$zu3(@;zZ>@h4fScCRa%!HHg6Wuln]JBT0(aY`g/,Rv mLt=b4H9C');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress.
 *
 * È possibile avere installazioni multiple su di un unico database
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Per gli sviluppatori: modalità di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * È fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all’interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta le variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');